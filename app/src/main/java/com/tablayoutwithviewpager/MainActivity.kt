package com.tablayoutwithviewpager

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var viewPagerAdapter: ViewPagerAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()

        viewPager!!.offscreenPageLimit = 3         //upto 3 off screen pages will not get destroyed. default is 1
        setSupportActionBar(toolBar)
        passFragmentsIntoAdapter()
        setAdapter()
        setIconsInTabLayout()
    }

    internal fun init() {

        viewPagerAdapter = ViewPagerAdapter(supportFragmentManager)

    }

    internal fun passFragmentsIntoAdapter() {            //can also be done with viewPagerStateAdapter in similar way
        viewPagerAdapter!!.addFragmentTabName(Frag1(), "Frag1")
        viewPagerAdapter!!.addFragmentTabName(Frag2(), "Frag2")
        viewPagerAdapter!!.addFragmentTabName(Frag3(), "Frag3")
    }

    internal fun setAdapter() {
        viewPager!!.adapter = viewPagerAdapter
        tabLayout!!.setupWithViewPager(viewPager)
    }

    internal fun setIconsInTabLayout() {         //should be called after setting adapter and tab layout
        //tab titles can also be set here using setText
        tabLayout!!.getTabAt(0)!!.setIcon(R.mipmap.ic_launcher)
        tabLayout!!.getTabAt(1)!!.setIcon(R.mipmap.ic_launcher)
        tabLayout!!.getTabAt(2)!!.setIcon(R.mipmap.ic_launcher)
    }

}


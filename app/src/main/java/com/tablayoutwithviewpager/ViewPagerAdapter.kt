package com.tablayoutwithviewpager

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import java.util.*

/**
 * Created by yudizsolutionspvtltd on 15/02/17.
 */

class ViewPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val fragments = ArrayList<Fragment>()
    private val tabNames = ArrayList<String>()

    fun addFragmentTabName(fragment: Fragment, tabName: String) {

        fragments.add(fragment)
        tabNames.add(tabName)

    }

    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int): CharSequence {            //to set title of tabs
        return tabNames[position]
    }
}
